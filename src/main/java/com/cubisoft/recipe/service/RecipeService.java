package com.cubisoft.recipe.service;

import com.cubisoft.recipe.domain.Recipe;

import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

public interface RecipeService {

    Set<Recipe> getRecipes();
}
