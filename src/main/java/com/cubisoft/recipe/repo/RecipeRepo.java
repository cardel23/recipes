package com.cubisoft.recipe.repo;

import com.cubisoft.recipe.domain.Recipe;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public interface RecipeRepo extends CrudRepository<Recipe, Long> {
}
