package com.cubisoft.recipe.repo;

import com.cubisoft.recipe.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public interface CategoryRepo extends CrudRepository<Category, Long> {

    Optional<Category> findByDescription(String description);

}
