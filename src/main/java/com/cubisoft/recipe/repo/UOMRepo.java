package com.cubisoft.recipe.repo;

import com.cubisoft.recipe.domain.UOM;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public interface UOMRepo extends CrudRepository<UOM, Long> {
    Optional<UOM> findByDescription(String description);
}
