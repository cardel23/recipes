package com.cubisoft.recipe.domain;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public enum Difficulty {
    BEGINNER, ADVANCED, EXPERT
}
