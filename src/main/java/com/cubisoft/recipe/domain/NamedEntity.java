package com.cubisoft.recipe.domain;

import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
@Entity
public abstract class NamedEntity extends BaseEntity implements Serializable {

    protected String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
