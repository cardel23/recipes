package com.cubisoft.recipe.controller;

import com.cubisoft.recipe.domain.Category;
import com.cubisoft.recipe.domain.UOM;
import com.cubisoft.recipe.repo.CategoryRepo;
import com.cubisoft.recipe.repo.UOMRepo;
import com.cubisoft.recipe.service.RecipeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
@Controller
@Slf4j
public class IndexController {

    private final RecipeService recipeService;

    public IndexController(RecipeService recipeService) {
        this.recipeService = recipeService;
    }

    @RequestMapping({"","/","/index","/index.html"})
    public String getIndexPage(Model model){

        model.addAttribute("recipes", recipeService.getRecipes());
        return "index";
    }
}
