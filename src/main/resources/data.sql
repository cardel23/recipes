
INSERT INTO category (description) VALUES ('American');
INSERT INTO category (description) VALUES ('Italian');
INSERT INTO category (description) VALUES ('Mexican');
INSERT INTO category (description) VALUES ('Fast Food');
INSERT INTO uom (description) VALUES ('Teaspoon');
INSERT INTO uom (description) VALUES ('Tablespoon');
INSERT INTO uom (description) VALUES ('Cup');
INSERT INTO uom (description) VALUES ('Pinch');
INSERT INTO uom (description) VALUES ('Dash');
INSERT INTO uom (description) VALUES ('Ounce');
INSERT INTO uom (description) VALUES ('Each');
INSERT INTO uom (description) VALUES ('Pint');