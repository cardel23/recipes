package com.cubisoft.recipe.repo;

import com.cubisoft.recipe.domain.UOM;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Test de integracion
 * Created by Carlos Delgadillo on 23/11/20
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UOMRepoIT {

    @Autowired UOMRepo uomRepo;

    @Before
    public void setUp() throws Exception {
    }

    @Test
//    @DirtiesContext recarga el contexto para la siguiente prueba
    public void findByDescription() {
        Optional<UOM> optionalUOM = uomRepo.findByDescription("Teaspoon");
        assertEquals("Teaspoon", optionalUOM.get().getDescription());
    }

    @Test
    public void findByDescriptionCup() {
        Optional<UOM> optionalUOM = uomRepo.findByDescription("Cup");
        assertEquals("Cup", optionalUOM.get().getDescription());
    }
}