package com.cubisoft.recipe.service;

import com.cubisoft.recipe.domain.Recipe;
import com.cubisoft.recipe.repo.RecipeRepo;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

public class RecipeServiceImplTest {

    RecipeServiceImpl recipeService;

    @Mock
    RecipeRepo recipeRepo;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        recipeService = new RecipeServiceImpl(recipeRepo);
    }

    @Test
    public void getRecipes(){
        Recipe recipe = new Recipe();
        Set set = new HashSet();
        set.add(recipe);

        when(recipeRepo.findAll()).thenReturn(set);

        Set<Recipe> recipes = recipeService.getRecipes();
        assertEquals(recipes.size(), 1);
        verify(recipeRepo, times(1)).findAll();
    }
}