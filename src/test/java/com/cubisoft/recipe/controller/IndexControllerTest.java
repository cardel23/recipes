package com.cubisoft.recipe.controller;

import com.cubisoft.recipe.domain.Recipe;
import com.cubisoft.recipe.service.RecipeServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

public class IndexControllerTest {

    @Mock RecipeServiceImpl recipeService;
    @Mock Model model;

    IndexController controller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        controller = new IndexController(recipeService);
    }

    @Test
    public void setMockMVC() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
        mockMvc.perform(get("/"))
                .andExpect(status().is(200))
                .andExpect(view().name("index"));
    }

    @Test
    public void getIndexPage() {

        Set set = new HashSet();
        set.add(new Recipe());
        set.add(new Recipe());

        when(recipeService.getRecipes()).thenReturn(set);

        ArgumentCaptor<Set<Recipe>> argumentCaptor = ArgumentCaptor.forClass(Set.class);


        String viewName = controller.getIndexPage(model);

        /**
         * Revisa que el valor que devuelve el metodo sea "index"
         */
        assertEquals("index", viewName);

        /**
         * Verifica que el metodo getRecipes() en el servicio se llame 1 vez
         */
        verify(recipeService, times(1)).getRecipes();

        /**
         * Verifica que el metodo addAtribute de la propiedad model se llame 1 vez y que este contenga
         * el valor "recipes"
         */
//        verify(model, times(1)).addAttribute(eq("recipes"),anySet());
        verify(model, times(1)).addAttribute(eq("recipes"),argumentCaptor.capture());

        Set<Recipe> recipes = argumentCaptor.getValue();

        assertEquals(2, recipes.size());
    }
}